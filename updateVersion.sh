#!/bin/sh

version=$(cat SharedCompendiums/module.json | jq -r .version)
nextVersion=$(echo ${version} | awk -F. -v OFS=. '{$NF++;print}')
downloadUrl="https://gitlab.com/api/v4/projects/28922120/packages/generic/SharedCompendiums/${nextVersion}/SharedCompendiums.zip"

json=$(jq ".version = \"${nextVersion}\"" SharedCompendiums/module.json)
echo $json | jq . > SharedCompendiums/module.json
json=$(jq ".download = \"${downloadUrl}\"" SharedCompendiums/module.json)
echo $json | jq . > SharedCompendiums/module.json
